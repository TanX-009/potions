import { createCamera, trackMouse } from "./components/camera";
import { createLights } from "./components/lights";
import { createScene } from "./components/scene";
import { loadModels } from "./components/World/loadModels";
import { createEnvironment } from "./components/environment";
import { createControls } from "./systems/controls";
import { Resizer } from "./systems/Resizer";
import { createRenderer } from "./systems/renderer";
import starrySkyShader from "./systems/shaders/starrySkyShader";
import { Loop } from "./systems/Loop";
import { createLoadingManager } from "./systems/loadingManager";
import { RaycasterSystem } from "./systems/Raycaster";
import { createTween } from "./systems/tween";
import { appendInfo } from "./systems/info";
import { FireFlies } from "./components/FireFlies";
import fireFliesShader from "./systems/shaders/fireFliesShader";
import { loadAudio } from "./systems/audio";

let camera;
let renderer;
let scene;
let loop;
let controls;
let loadingManager;
let environment;
let raycasterSystem;
let tween;
let infoContainer;
let fireFlies;
let is_mobile;

class World {
  constructor(container, loadingScreen, info_container) {
    is_mobile =
      !!navigator.userAgent.match(/iphone|android|blackberry/gi) || false;
    camera = createCamera(container, is_mobile);
    renderer = createRenderer();
    scene = createScene();
    environment = createEnvironment(starrySkyShader);
    container.append(renderer.domElement);
    controls = createControls(camera, renderer.domElement, scene, is_mobile);
    loadingManager = createLoadingManager(loadingScreen);
    tween = createTween();
    fireFlies = new FireFlies(fireFliesShader);
    infoContainer = info_container;

    raycasterSystem = new RaycasterSystem(container, scene, camera);

    loop = new Loop(camera, scene, renderer, raycasterSystem, is_mobile);

    if (!is_mobile) {
      trackMouse(container, camera);
    }

    loop.updatables.push(controls, camera, tween, fireFlies.fireFliesMaterial);

    // components
    const { directionalLight, ambientLight } = createLights();

    scene.add(directionalLight, ambientLight, environment, fireFlies.fireFlies);

    const resizer = new Resizer(container, camera, renderer);
    resizer.onResize = () => {
      this.render();
      if (!is_mobile) {
        trackMouse(container, camera);
      }
    };
  }
  async init() {
    // Models loading
    const { potionBar } = await loadModels(loadingManager);

    const { glass } = await loadAudio(loadingManager, camera);

    // AnimatatedModels addTo loop(updatables)
    loop.updatables.push(potionBar.model);

    raycasterSystem.container.onclick = (event) =>
      raycasterSystem.openInfo(event, scene, appendInfo, infoContainer, glass);

    // Models addTo scene
    scene.add(potionBar.model, glass);
  }
  render() {
    renderer.render(scene, camera);
    raycasterSystem.raycaster.setFromCamera(
      raycasterSystem.pointer,
      raycasterSystem.camera
    );
  }

  start() {
    loop.start();
  }

  stop() {
    loop.stop();
  }
}
export { World };
