import { Raycaster, Vector2 } from "three";

class RaycasterSystem {
  constructor(container, scene, camera) {
    const raycaster = new Raycaster();
    const pointer = new Vector2();
    this.raycaster = raycaster;
    this.container = container;
    this.camera = camera;
    this.pointer = pointer;
  }
  openInfo(event, scene, appendInfo, infoContainer, glass) {
    this.pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
    this.pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;

    this.raycaster.setFromCamera(this.pointer, this.camera);

    const intersects = this.raycaster.intersectObject(scene, true);
    if (intersects.length > 0) {
      for (let objects in intersects) {
        if (/_/.test(intersects[objects].object.name)) {
          let targetId = intersects[objects].object.name.charAt(
            intersects[objects].object.name.length - 1
          );
          if (targetId == "") {
            targetId = -1;
          } else if (targetId == 0) {
            targetId = 10;
            glass.play();
            appendInfo(targetId, infoContainer);
          } else if (/[1-9]/.test(targetId)) {
            glass.play();
            appendInfo(targetId, infoContainer);
          }
          break;
        }
      }
    }
  }
}

export { RaycasterSystem };
