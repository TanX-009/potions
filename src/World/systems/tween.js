import { TWEEN } from "three/examples/jsm/libs/tween.module.min";

function createTween() {
  const tween = TWEEN;

  tween.tick = (delta, elapsedTime) => {
    tween.update();
  };

  return tween;
}

export { createTween };
