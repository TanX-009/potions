function appendInfo(assetIdentifier, container) {
  const potionTitle = {
    1: "Night Vision Potion",
    2: "Building Potion",
    3: "Strength Potion",
    4: "Dead Potion",
    5: "Boiling Blood Potion",
    6: "Vision Potion",
    7: "Love Potion",
    8: "Cleaning Potion",
    9: "Weather Resistance Potion",
    10: "Tomato Potion",
  };
  const potionTitleColor = {
    1: "#ffb326",
    2: "#fc566f",
    3: "#ff4242",
    4: "#996eff",
    5: "darkred",
    6: "#32fc7d",
    7: "#ff0059",
    8: "#4a98ff",
    9: "#5c0aff",
    10: "tomato",
  };
  const potionInfo = {
    1: "See everything in the night even the things you don't want to see.",
    2: "Ever seen minecraft builders build, dang boiii they build fast and crazy good looking.<br>Want to build something like these guys do, have this potion.",
    3: "Ever had enemies, with this potion you are no match to them.",
    4: "Is this a potion where you die or is this the one that gives you strength of the dead ?<br>Hmm..<br>God knows, anyways try and tell me I'm curious.",
    5: "Blood....Boiling....?",
    6: "Where did the eyes come from ? Is this sugary ?",
    7: "You peeky, you know what this is for ?",
    8: "No you dumbass. It's just soap and water.<br> Don't know if you are supposed to drink.<br> Well I know you'll figure it out.",
    9: "No raincoat, sunglasses, even clothes!<br> No problem have this and wear nothing.",
    10: "Who doesn't love tomato soup ? Grandma says it cures.<br> Hence it a potion, no questions asked.",
  };
  container.insertAdjacentHTML(
    "beforeend",
    `
  <div id="info">
		<button id="close">&#x2715</button>
		<img class="potion-img" src="images/Potion_${assetIdentifier}.png" alt="Potion_${assetIdentifier}"/>
		<div class="potion-info">
			<h2 style="color: ${potionTitleColor[assetIdentifier]};">${potionTitle[assetIdentifier]}</h2>
			<p>${potionInfo[assetIdentifier]}</p>
		</div>
  </div>
  `
  );

  let is_mobile =
    !!navigator.userAgent.match(/iphone|android|blackberry/gi) || false;

  if (!is_mobile) {
    container.children["info"].style.background = "none";
    container.children["info"].classList.add("info-mob");
  }

  const closeBTN = container.children["info"].children["close"];
  closeBTN.onclick = () => {
    container.children["info"].style.animation = "remove-animate 0.3s linear";
    container.children["info"].addEventListener("animationend", () => {
      container.children["info"].remove();
    });
  };
}

export { appendInfo };
