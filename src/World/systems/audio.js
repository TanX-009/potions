import { Audio, AudioListener, AudioLoader } from "three";

async function loadAudio(loadingManager, camera) {
  const loader = new AudioLoader(loadingManager);
  const listener = new AudioListener();
  camera.add(listener);

  const [glassData] = await Promise.all([
    loader.loadAsync("audio/glass-knock.mp3"),
  ]);

  const glass = new Audio(listener);
  glass.setBuffer(glassData);
  glass.setVolume(0.2);

  return { glass };
}

export { loadAudio };
