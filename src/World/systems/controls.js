import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

function createControls(camera, canvas, scene, is_mobile) {
  const controls = new OrbitControls(camera, canvas);

  if (!is_mobile) {
    controls.enableRotate = false;
  }

  controls.enableDamping = true;
  controls.enablePan = false;
  controls.enableZoom = true;

  controls.minDistance = 1;
  controls.maxDistance = 3;
  controls.minAzimuthAngle = -Math.PI / 2;
  controls.maxAzimuthAngle = Math.PI / 2;
  controls.minPolarAngle = -Math.PI / 2;
  controls.maxPolarAngle = (Math.PI * 3) / 5;

  controls.target.set(0.35, 0.7, 0);
  controls.tick = (delta, elapsedTime) => {
    controls.update(delta);
  };

  return controls;
}

function controlsTargetOn(controls, tween, target) {
  const coords = {
    x: controls.target.x,
    y: controls.target.y,
    z: controls.target.z,
  };
  new tween.Tween(coords)
    .to({ x: target.x, y: target.y, z: target.z })
    .onUpdate(() => controls.target.set(coords.x, coords.y, coords.z))
    .start();
}

export { createControls, controlsTargetOn };
