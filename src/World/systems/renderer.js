import { LinearToneMapping, sRGBEncoding, WebGLRenderer } from "three";

function createRenderer() {
  const renderer = new WebGLRenderer({
    antialias: true,
  });

  renderer.outputEncoding = sRGBEncoding;

  renderer.autoClear = false;
  renderer.physicallyCorrectLights = true;
  renderer.shadowMap.enabled = true;

  return renderer;
}

export { createRenderer };
