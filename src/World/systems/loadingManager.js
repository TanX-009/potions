import { LoadingManager } from "three";

function createLoadingManager(loadingScreen) {
  const loadingManager = new LoadingManager();
  const progressBar =
    loadingScreen.children["loading-bar"].children["progress-bar"];

  loadingManager.onLoad = function () {
    loadingScreen.classList.add("fade-out");

    loadingScreen.addEventListener("animationend", (event) => {
      event.target.remove();
    });
  };

  return loadingManager;
}

export { createLoadingManager };
