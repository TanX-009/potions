import { Clock } from "three";

const clock = new Clock();

class Loop {
  constructor(camera, scene, renderer, raycasterSystem, is_mobile) {
    this.camera = camera;
    this.scene = scene;
    this.renderer = renderer;
    this.raycasterSystem = raycasterSystem;
    this.updatables = [];
    is_mobile ? (this.pixelRatio = 0.7) : (this.pixelRatio = 1);
  }

  start() {
    this.renderer.setAnimationLoop(() => {
      this.tick();
      this.renderer.setPixelRatio(this.pixelRatio);
      this.renderer.render(this.scene, this.camera);
      this.raycasterSystem.raycaster.setFromCamera(
        this.raycasterSystem.pointer,
        this.raycasterSystem.camera
      );
    });
  }

  stop() {
    this.renderer.setAnimationLoop(null);
  }

  tick() {
    const delta = clock.getDelta();
    const elapsedTime = clock.getElapsedTime();
    for (const object of this.updatables) {
      object.tick(delta, elapsedTime);
    }
  }
}

export { Loop };
