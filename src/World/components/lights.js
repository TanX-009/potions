import { DirectionalLight, AmbientLight } from "three";

function createLights() {
  const directionalLight = new DirectionalLight("white", 1);
  directionalLight.position.set(0, 2, 0);

  const ambientLight = new AmbientLight("white", 0.1);
  return { directionalLight, ambientLight };
}

export { createLights };
