import { PerspectiveCamera } from "three";

const camX = 0.6,
  camY = 1.1,
  camZ = 1.4;
let perspective = 60;

function createCamera(container, is_mobile) {
  if (is_mobile) {
    perspective = 75;
  }
  const camera = new PerspectiveCamera(
    perspective,
    container.offsetWidth / container.offsetHeight,
    0.01,
    1000
  );
  camera.position.set(camX, camY, camZ);
  camera.tick = () => {};

  return camera;
}

function trackMouse(container, camera) {
  let mouseX = 0,
    mouseY = 0;

  container.addEventListener("pointermove", (event) => {
    mouseX =
      (event.clientX - container.offsetWidth / 2) / container.offsetWidth / 2;
    mouseY =
      (event.clientY - container.offsetHeight / 2) / container.offsetHeight / 2;
  });

  camera.tick = () => {
    camera.position.x += (mouseX + camX - camera.position.x) * 0.036;
    camera.position.y += (-mouseY + camY - camera.position.y) * 0.036;
  };
}

export { createCamera, trackMouse };
