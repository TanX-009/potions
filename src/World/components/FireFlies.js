import {
  AdditiveBlending,
  BufferAttribute,
  BufferGeometry,
  Points,
  ShaderMaterial,
  Vector3,
} from "three";
class FireFlies {
  constructor(fireFliesShader) {
    const firefliesGeometry = new BufferGeometry();
    const firefliesCount = 60;
    const positionArray = new Float32Array(firefliesCount * 3);
    const scaleArray = new Float32Array(firefliesCount);
    for (let i = 0; i < firefliesCount; i++) {
      new Vector3(
        (Math.random() - 0.5) * 4,
        Math.random() * 1.5,
        (Math.random() - 0.5) * 4
      ).toArray(positionArray, i * 3);
      scaleArray[i] = Math.random();
      scaleArray[i] = Math.random();
    }
    firefliesGeometry.setAttribute(
      "position",
      new BufferAttribute(positionArray, 3)
    );

    firefliesGeometry.setAttribute(
      "aScale",
      new BufferAttribute(scaleArray, 1)
    );

    const firefliesMaterial = new ShaderMaterial({
      vertexShader: fireFliesShader.vertexShader,
      fragmentShader: fireFliesShader.fragmentShader,
      transparent: true,
      uniforms: {
        uTime: { value: 0 },
        uPixelRatio: { value: Math.min(window.devicePixelRatio, 2) },
        uSize: { value: 100 },
      },

      blending: AdditiveBlending,
      depthWrite: false,
    });

    firefliesMaterial.tick = (delta, elapsedTime) => {
      firefliesMaterial.uniforms.uTime.value = elapsedTime;
    };

    const fireflies = new Points(firefliesGeometry, firefliesMaterial);

    this.fireFliesMaterial = firefliesMaterial;
    this.fireFliesGeometry = firefliesGeometry;
    this.fireFlies = fireflies;
  }
}

export { FireFlies };
