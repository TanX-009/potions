import { Color, Fog, Scene } from "three";

function createScene() {
  const scene = new Scene();

  scene.background = new Color("#020203");

  return scene;
}

export { createScene };
