import { AnimationMixer } from "three";

class Model {
  constructor(data, model_identifier = 0) {
    const actions = {};
    const objects = {};

    if (typeof model_identifier === "number") {
      this.model = data.scene.children[model_identifier];
    } else if (typeof model_identifier === "string") {
      for (let model in data.scene.children) {
        if (data.scene.children[model].name == model_identifier) {
          this.model = data.scene.children[model];
        }
      }
    } else {
      throw "Invalid model_identifier";
    }

    const mixer = new AnimationMixer(this.model);

    this.data = data;
    this.actions = actions;
    this.objects = objects;
    this.mixer = mixer;
  }

  setNestedObjectPosition(targetName, x, y, z) {
    this.model.traverse((child) => {
      if (child.name == targetName) {
        child.position.set(x, y, z);
      }
    });
  }

  animate() {
    // get animations clip from model data
    const clips = {};
    for (let clipNo in this.data.animations) {
      clips[Object.values(this.data.animations)[clipNo].name] = Object.values(
        this.data.animations
      )[clipNo];
    }

    // add all actions to this.actions
    for (let clip in clips) {
      this.actions[clip] = this.mixer.clipAction(clips[clip]);
    }

    this.model.tick = (delta, elapsedTime) => this.mixer.update(delta);
  }

  playAction(action_identifier = null) {
    if (action_identifier instanceof RegExp) {
      for (let action in this.actions) {
        if (action_identifier.test(action)) {
          this.actions[action].play();
        }
      }
    } else {
      if (action_identifier == null) {
        for (let action in this.actions) {
          this.actions[action].play();
        }
      } else if (typeof action_identifier == "object") {
        for (let action in action_identifier) {
          if (action_identifier[action] instanceof RegExp) {
            for (let actionFromActions in this.actions) {
              if (action_identifier[action].test(actionFromActions)) {
                this.actions[actionFromActions].play();
              }
            }
          } else {
            if (typeof action_identifier[action] == "string") {
              this.actions[action_identifier[action]].play();
            } else if (typeof action_identifier[action] == "number") {
              this.actions[
                Object.keys(this.actions)[action_identifier[action]]
              ].play();
            }
          }
        }
      } else if (typeof action_identifier == "string") {
        this.actions[action_identifier].play();
      } else if (typeof action_identifier == "number") {
        this.actions[Object.keys(this.actions)[action_identifier]].play();
      } else {
        throw "Invalid action_identifier";
      }
    }
  }

  haltAction(action_identifier = null, duration) {
    if (action_identifier instanceof RegExp) {
      for (let action in this.actions) {
        if (action_identifier.test(action)) {
          this.actions[action].halt(duration);
        }
      }
    } else {
      if (action_identifier == null) {
        for (let action in this.actions) {
          this.actions[action].halt(duration);
        }
      } else if (typeof action_identifier == "object") {
        for (let action in action_identifier) {
          if (action_identifier[action] instanceof RegExp) {
            for (let actionFromActions in this.actions) {
              if (action_identifier[action].test(actionFromActions)) {
                this.actions[actionFromActions].halt(duration);
              }
            }
          } else {
            if (typeof action_identifier[action] == "string") {
              this.actions[action_identifier[action]].halt(duration);
            } else if (typeof action_identifier[action] == "number") {
              this.actions[
                Object.keys(this.actions)[action_identifier[action]]
              ].halt(duration);
            }
          }
        }
      } else if (typeof action_identifier == "string") {
        this.actions[action_identifier].halt(duration);
      } else if (typeof action_identifier == "number") {
        this.actions[Object.keys(this.actions)[action_identifier]].halt(
          duration
        );
      } else {
        throw "Invalid action_identifier";
      }
    }
  }

  enableCastShadow() {
    this.model.traverse(function (child) {
      if (child.isMesh) {
        child.castShadow = true;
      }
    });
  }
  enableRecieveShadow() {
    this.model.traverse(function (child) {
      if (child.isMesh) {
        child.recieveShadow = true;
      }
    });
  }
  disableFrustumCulled() {
    this.model.traverse(function (child) {
      if (child.isMesh) {
        child.frustumCulled = false;
      }
    });
  }
}

export { Model };
