import {
  Color,
  DoubleSide,
  Mesh,
  ShaderMaterial,
  SphereGeometry,
  Vector3,
} from "three";

function createEnvironment(shader) {
  const skyDomeRadius = 900.01;
  const sphereMaterial = new ShaderMaterial({
    uniforms: {
      skyRadius: { value: skyDomeRadius },
      env_c1: { value: new Color("#0d1a2f") },
      env_c2: { value: new Color("#0f8682") },
      noiseOffset: { value: new Vector3(100.01, 100.01, 100.01) },
      starSize: { value: 0.01 },
      starDensity: { value: 0.09 },
      clusterStrength: { value: 0.2 },
      clusterSize: { value: 0.2 },
    },
    vertexShader: shader.vertexShader,
    fragmentShader: shader.fragmentShader,
    side: DoubleSide,
  });
  const sphereGeometry = new SphereGeometry(skyDomeRadius, 20, 20);
  const skyDome = new Mesh(sphereGeometry, sphereMaterial);
  skyDome.name = "Sky";

  return skyDome;
}

export { createEnvironment };
