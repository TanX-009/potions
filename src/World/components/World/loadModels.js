import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader.js";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";

import { Model } from "../Model.js";

async function loadModels(loadingManager) {
  const loader = new GLTFLoader(loadingManager);
  const dracoLoader = new DRACOLoader();
  dracoLoader.setDecoderPath("draco/gltf/");
  loader.setDRACOLoader(dracoLoader);

  const [potionBarData] = await Promise.all([
    loader.loadAsync("models/PotionBar.glb"),
  ]);

  const potionBar = new Model(potionBarData);
  potionBar.model.rotation.y = -Math.PI / 2;
  potionBar.model.position.set(0, -1, 0);
  potionBar.animate();
  potionBar.playAction();

  for (let action in potionBar.actions) {
    potionBar.actions[action].timeScale = 0.6;
  }

  return { potionBar };
}

export { loadModels };
