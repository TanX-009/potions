import { World } from "./World/World.js";

// TO-DO
// remove loadingscreen here

async function main() {
  const container = document.querySelector("#scene-container");
  const LoadingScreen = document.getElementById("loading-screen");
  const infoContainer = document.getElementById("info-container");

  const infoBtn = document.getElementById("info-btn");
  infoBtn.onclick = () => {
    infoBtn.insertAdjacentHTML(
      "afterend",
      `
			<div id="site-info">
				<button id="site-info-close-btn">&#10006</button>
			  <h3>Potions!</h3>
				<h5>Created by - Tanmay Muley<br />aka<br />TanX-009</h5>

      <p
        xmlns:cc="http://creativecommons.org/ns#"
        xmlns:dct="http://purl.org/dc/terms/"
      >
        <a
          href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1"
          target="_blank"
          rel="license noopener noreferrer"
          style="display: inline-block"
          >CC BY-SA 4.0<br><img
            style="
              height: 22px !important;
              margin-left: 3px;
              vertical-align: text-bottom;
            "
            src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" /><img
            style="
              height: 22px !important;
              margin-left: 3px;
              vertical-align: text-bottom;
            "
            src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" /><img
            style="
              height: 22px !important;
              margin-left: 3px;
              vertical-align: text-bottom;
            "
            src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"
        /></a>
      </p>
		  </div>
			`
    );
    const siteInfoCloseBtn = document.getElementById("site-info-close-btn");
    siteInfoCloseBtn.onclick = () => {
      siteInfoCloseBtn.parentNode.style.animation =
        "remove-animate 0.1s forwards";
      siteInfoCloseBtn.parentNode.addEventListener("animationend", () => {
        siteInfoCloseBtn.parentNode.remove();
      });
    };
  };

  const world = new World(container, LoadingScreen, infoContainer);

  await world.init();

  world.start();
}

main().catch((err) => {
  console.error(err);
});
